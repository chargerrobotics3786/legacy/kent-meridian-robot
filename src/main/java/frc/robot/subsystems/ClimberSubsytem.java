// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

//i am 90% sure we are using double solenoids

package frc.robot.subsystems;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import static edu.wpi.first.wpilibj.DoubleSolenoid.Value.*;


public class ClimberSubsytem extends SubsystemBase {
  /** Creates a new PneumaticSubsytem. */
  public ClimberSubsytem() {}

  //defines double solenoid. port 6 makes the cylinder go up, 7 makes it go down.
  DoubleSolenoid kickerSolenoid = new DoubleSolenoid(PneumaticsModuleType.CTREPCM, 6, 7);

  public void toggleSolenoid(){
    //method toggles the solenoid which changes from off to on or on to off
    kickerSolenoid.toggle();
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
    kickerSolenoid.set(kOff); //double solenoids have to be set before they can be toggled
  }
}
